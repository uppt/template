package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.InstanceRunner;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.UUID;

/**
 * Created by malte on 02.02.17.
 */
@RunWith(VertxUnitRunner.class)
public class PingService1Test {
    // Note: as this test class is not a UPPT class, the Vert.x methods for accessing the EventBus must be used

    // location of the used config files
    private static String configDirectory = "test" + File.separator + "pingService1" + File.separator + "config" + File.separator;

    private static Vertx vertx = null;
    private static EventBus eventBus = null;

    @BeforeClass
    public static void setupLocalVertx(TestContext context) {
        Async setupResult = context.async(1);

        InstanceRunner.setConfigDirectory(configDirectory);
        // note that both the ping and the pong service are launched - the ping service depends on the pong service
        // so the instance would not start without this dependency being resolved
        // another (cleaner way) would be to implement and use a dummy service for pingpong.1.newPing
        vertx = InstanceRunner.vertx(new JsonArray()
                .add("pingService1.json")
                .add("pongService1.json"));
        eventBus = vertx.eventBus();

        // wait until the vertx instance and all services are up and running
        MessageConsumer<JsonObject> consumer = eventBus.consumer("system.1.newInstance", message -> {
            // startup complete
            if ("200".equalsIgnoreCase(message.headers().get("statusCode"))) {
                context.assertEquals("200", message.headers().get("statusCode"));
                context.assertNotNull(message.headers().get("correlationID"));
                context.assertNotNull(message.headers().get("origin"));
                setupResult.countDown();
            }
        });

        setupResult.awaitSuccess();
        consumer.unregister();
    }

    @Test
    public void testPingReception(TestContext context) {
        // register a processor for the pings - in addition to the pong service's
        // due to the used round robin-based distribution, both the pong service as well as this process will receive pings

        // wait until 2 pings have been received
        Async setupResult = context.async(4);

        MessageConsumer<JsonObject> processor = eventBus.consumer("pingpong.1.newPing", message -> {
            // received a ping
            if ("200".equalsIgnoreCase(message.headers().get("statusCode"))) {
                context.assertEquals("200", message.headers().get("statusCode"));
                context.assertNotNull(message.headers().get("correlationID"));
                context.assertNotNull(message.headers().get("origin"));
                setupResult.countDown();

                message.reply(new JsonObject(),new DeliveryOptions()
                        .addHeader("statusCode", "200")
                        .addHeader("correlationID", message.headers().get("correlationID"))
                        .addHeader("origin", "PingService1Test")
                );
            }
        });

        setupResult.awaitSuccess();
        processor.unregister();
    }

    @AfterClass
    public static void tearDownVertx(TestContext context) {
        try {
            vertx.close();
        }
        catch(Exception e) {}
    }
}
