package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.InstanceRunner;
import de.maltebehrendt.uppt.events.Message;
import de.maltebehrendt.uppt.events.Impl.DummyMessageImpl;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

/**
 * Created by malte on 02.02.17.
 */
@RunWith(VertxUnitRunner.class)
public class PongService1Test {

    // location of the used config files
    private static String configDirectory = "test" + File.separator + "pongService1" + File.separator + "config" + File.separator;

    private static Vertx vertx = null;
    private static EventBus eventBus = null;

    @BeforeClass
    public static void setupLocalVertx(TestContext context) {
        Async setupResult = context.async(1);

        InstanceRunner.setConfigDirectory(configDirectory);
        // note that both the ping and the pong service are launched - the ping service depends on the pong service
        // so the instance would not start without this dependency being resolved
        // another (cleaner way) would be to implement and use a dummy service for pingpong.1.newPing
        vertx = InstanceRunner.vertx(new JsonArray()
                .add("noopService1.json"));
        eventBus = vertx.eventBus();

        // wait until the vertx instance and all services are up and running
        MessageConsumer<JsonObject> consumer = eventBus.consumer("system.1.newInstance", message -> {
            // startup complete
            if ("200".equalsIgnoreCase(message.headers().get("statusCode"))) {
                context.assertEquals("200", message.headers().get("statusCode"));
                context.assertNotNull(message.headers().get("correlationID"));
                context.assertNotNull(message.headers().get("origin"));
                setupResult.countDown();
            }
        });

        setupResult.awaitSuccess();
        consumer.unregister();
    }

    @Test
    public void testReplyOnPing(TestContext context) {
        Async setupResult = context.async(2);
        // As the pong service only implements a processor (without sending messages, that is it's not a aggregator or customer)
        // it can be unit tested (well, more like it; without using the event bus). However, logging needs to be taken care of


        // Initialize the service's class
        PongService1 pongService1 = new PongService1();
        pongService1.setLogger(LoggerFactory.getLogger(PongService1.class));
        pongService1.setVertx(vertx);

        vertx.executeBlocking(future -> {
            pongService1.prepare(future);
        }, result -> {
            context.assertTrue(result.succeeded(), "Service executed prepare() successfully.");
            setupResult.countDown();

            // that that the service is set up, continue with testing

            // Write a check on the expected result
            Handler<Message> dummyReplyHandler = new Handler<Message>() {
                @Override
                public void handle(Message event) {
                    context.assertEquals(200, event.statusCode());
                    setupResult.countDown();
                }
            };
            // Call the processor method with a prepared message
            Message dummyMessage = new DummyMessageImpl(dummyReplyHandler);
            pongService1.processNewPing(dummyMessage);
        });


        setupResult.awaitSuccess();
    }
}
