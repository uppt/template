package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;

/**
 * Created by malte on 02.02.17.
 */
public class PongService1 extends AbstractService {

    @Override
    public void prepare(Future<Object> prepareFuture) {
        // run any setup code before the processors/customers are linked to the system
        prepareFuture.complete();
    }

    @Override
    public void startConsuming() {
        // when called, processors/customers are active (added to the system)
        // and you can either for processors to receive messages or send own messaged
        // using customers
    }

    // most simple example of a processor method
    @Processor(
            domain = "pingpong",
            version = "1",
            type = "newPing",
            description = "Replies to a ping with a pong (empty reply message with status code 200)"
    )
    public void processNewPing(Message message) {
        message.reply(200);
        logger.info("["+ message.correlationID() + "] Received ping from " + message.origin());
    }

    public void setVertx(Vertx newVertx) {
        vertx = newVertx;
    }

    public void setLogger(Logger newLogger) {
        logger = newLogger;
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        // close your service's resources; counterpart to prepare()
        shutdownFuture.complete();
    }
}
