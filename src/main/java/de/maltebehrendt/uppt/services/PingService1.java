package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Customer;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

/**
 * Created by malte on 02.02.17.
 */
public class PingService1 extends AbstractService {
    private long periodicID = 0;


    @Override
    public void prepare(Future<Object> prepareFuture) {
        // signal that preparation is done by calling the complete function
        prepareFuture.complete();
    }

    @Override
    public void startConsuming() {
        // when called, processors/customers are active (added to the system)
        // and you can either for processors to receive messages or send own messaged
        // using customers

        // call the customer ping every second
        periodicID = vertx.setPeriodic(1000, event -> {
            sendNewPingEvent();
        });
    }

    // most simple example of a customer
    @Customer(
            domain = "pingpong",
            version = "1",
            type = "newPing",
            description = "Sends a ping and expects a pong (reply message with status code 200)"
    )
    public void sendNewPingEvent() {
        send("pingpong", "1", "newPing", new JsonObject(), reply -> {
            if(reply.succeeded() && reply.statusCode() == 200) {
                logger.info("[" + reply.correlationID() + "] Received pong from " + reply.origin());
            }
            else {
                logger.error("[" + reply.correlationID() + "] Failed to receive pong for ping. Origin: " + reply.origin() + " Cause: " + reply.cause());
            }
        });
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        // close your service's resources; counterpart to prepare()

        if(periodicID != 0) {
            vertx.cancelTimer(periodicID);
        }
        shutdownFuture.complete();
    }
}
